package exercicis_piles_cues;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Scanner;

public class Parentesis {
	public static final char[][] parentesis = {{'(','[','{'},{')',']','}'}};
	
	private static boolean esParentesi(char c, int fila) {
		boolean found = false;
		int col=0;
		while (!found && col<parentesis[0].length) {
			if (parentesis[fila][col] == c)
				found=true;
			col++;
		}
		return found;
	}
	
	public static boolean esParentesiObert(char c) {
		return esParentesi(c, 0);
	}
	
	public static boolean esParentesiTancat(char c) {
		return esParentesi(c, 1);
	}
	
	public static Character obertATancat(char c) {
		Character tancat=null;
		int col=0;
		while (tancat==null && col<parentesis[0].length) {
			if (parentesis[0][col]==c) {
				tancat=parentesis[1][col];
			}
			col++;
		}
		return tancat;
	}
	
	public void comprova(String expressio) throws MalaExpressioException {
		Deque<Character> pila = new LinkedList<Character>();
		char c, obert;
		for (int i=0; i<expressio.length(); i++) {
			c = expressio.charAt(i);
			if (esParentesiObert(c)) {
				pila.push(c);
			} else if (esParentesiTancat(c)) {
				if (pila.isEmpty()) {
					throw new MalaExpressioException("Error: "+c+" inesperat.", i);
				}
				obert = pila.pop();
				if (obertATancat(obert)!=c) {
					throw new MalaExpressioException("Error: s'esperava "+obertATancat(obert)+
							" i s'ha trobat "+c+".", i);
				}
			}
		}
		if (!pila.isEmpty()) {
			throw new MalaExpressioException("Error: s'esperava "+obertATancat(pila.pop())+
					" i s'ha trobat el final de l'expressió.", expressio.length());
		}
	}

	public static void main(String[] args) {
		Parentesis p = new Parentesis();
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Escriu una expressió matemàtica: ");
			String expr = sc.nextLine();
			try {
				p.comprova(expr);
				System.out.println("Expressió correcta.");
			} catch (MalaExpressioException e) {
				System.out.println(expr);
				for (int i=0; i<e.pos; i++)
					System.out.print(" ");
				System.out.println("^");
				System.out.println(e.getMessage());
			}
		}
	}

	public class MalaExpressioException extends Exception {
		private static final long serialVersionUID = 1L;
		public final int pos;

		public MalaExpressioException(int pos) {
			super();
			this.pos=pos;
		}

		public MalaExpressioException(String arg0, Throwable arg1, boolean arg2, boolean arg3, int pos) {
			super(arg0, arg1, arg2, arg3);
			this.pos=pos;
		}

		public MalaExpressioException(String arg0, Throwable arg1, int pos) {
			super(arg0, arg1);
			this.pos=pos;
		}

		public MalaExpressioException(String arg0, int pos) {
			super(arg0);
			this.pos=pos;
		}

		public MalaExpressioException(Throwable arg0, int pos) {
			super(arg0);
			this.pos=pos;
		}
	}
}
