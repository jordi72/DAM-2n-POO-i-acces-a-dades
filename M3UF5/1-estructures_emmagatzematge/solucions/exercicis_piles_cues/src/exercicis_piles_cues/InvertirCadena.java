package exercicis_piles_cues;

import java.util.Deque;
import java.util.LinkedList;

public class InvertirCadena {
	public static String inverteix(String cadena) {
		String invertida="";
		Deque<Character> pila = new LinkedList<>();
		for (int i=0; i<cadena.length(); i++) {
			pila.addFirst(cadena.charAt(i));
		}
		while (!pila.isEmpty()) {
			invertida+=pila.removeFirst();
		}
		return invertida;
	}
	
	public static String inverteix2(String cadena) {
		StringBuilder sb = new StringBuilder();
		Deque<Character> pila = new LinkedList<>();
		cadena.chars().forEach(c->pila.addFirst((char)c));
		pila.forEach(c->sb.append(c));
		return sb.toString();
	}

	public static void main(String[] args) {
		System.out.println(inverteix("Prova inversió cadena"));
		System.out.println(inverteix2("Prova inversió cadena"));
	}

}
