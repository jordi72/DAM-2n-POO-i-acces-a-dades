= POO. Biblioteques de classes fonamentals
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Interfícies gràfiques d'usuari

=== Introducció a les interfícies gràfiques en Java

Des que el Java va aparèixer a mitjan dels noranta, la forma d'implementar
interfícies gràfiques ha anat evolucionant. Per tal de mantenir la
compatibilitat amb versions anteriors, les classes originals no s'han
eliminat de les API, de manera que ara mateix conviuen diversos sistemes de
finestres:

* **AWT** (*Abstract Window Toolkit*): és el conjunt de original proposat pels
creadors de Java per a la construcció d'interfícies gràfiques. Com que
Swing es va crear a partir d'ampliar la base que oferia l'AWT (sovint les
classes de SWing són derivades directament de les seves classes equivalents
d'AWT), algunes de les classes de l'AWT encara s'utilitzen si es treballa
en Swing.

* **Swing**: fins fa poc era el conjunt d'eines recomanat per Oracle per a
implementar interfícies gràfiques en Java. A diferència de l'AWT, les classes
de Swing estan implementades en Java i són, per tant, independents del
sistema operatiu en què s'executen.

* **JavaFX**: la plataforma JavaX és un altre conjunt de classes per a crear
interfícies gràfiques en Java. El JavaFX incorpora elements com la
capacitat d'executar-se en un navegador web. A més suporta animacions i
gràfics tridimensionals, i es pot executar en sistemes mòbils com Android,
iOS i Raspberry Pi, entre d'altres.
+
Inicialment, JavaFX es distribuïa de forma separada al JRE/JDK. Però a
partir de la versió 8 de Java, ja forma part de les API estàndard. A més,
Oracle recomana que els nous programes es desenvolupin en JavaFX en comptes
de Swing. Encara que es preveu que Swing serà suportat durant molt de
temps, Oracle ha deixat clar que qualsevol nou desenvolupament es
realitzaria exclusivament a JavaFX.

* **SWT** (*Standard Widget Toolkit*): aquest és un conjunt d'eines per a
construir interícies gràfiques que no està inclòs a la JRE/JDK, però que
mereix especial atenció degut a què hi ha projectes prou importants que
l'utilitzen, entre d'ells, Eclipse (que són els que mantenen SWT
actualment) i diversos projectes d'IBM (que en són els creadors originals).
+
El projecte *RAP* (*Remote Application Platform*) uneix SWT amb altres
tecnologies per al desenvolupament d'aplicacions en xarxa.

=== Scene builder

Les interfícies gràfiques en JavaFX es poden crear mitjançant codi Java, o
mitjançant un fitxer XML.

L'Scene Builder és un programa que proporciona Oracle i que permet generar
els fitxers XML d'una interfície gràfica de forma visual.

Per tal de treballar amb JavaFX necessitarem configurar l'entorn de treball.

==== Configuració de l'Eclipse

===== Debian GNU/Linux

- Instal·lar el paquet *openjfx*.

- A l'Eclipse, instal·lar *e(fx)clipse install*

Per tal de tenir la documentació de Java i de JavaFX en local:

- Instal·lar els paquets *openjdk-8-doc* i *libopenjfx-java-doc*.

Per indicar a l'Eclipse on és la documentació de JavaFX:

- Anar a *Window->Preferences->Java->Installed JREs->java-8-openjdk-amd64->Edit...*

- Desplegar *...jfxrt.jar->Javadoc Location...*

- Posar: *file:/usr/share/doc/libopenjfx-java/api/*

===== Windows

Assegurar-se que l'Eclipse utilitza Java 8 per defecte:

- Anar a *Window->Preferences->Java->Installed JREs*

Si no està utilitzant Java 8:

- Afegir *Standard VM* i seleccionar el directori d'instal·lació del JDK 8.

- Assegurar-se que és el que utilitza per defecte.

- A *Java->Compiler* assegurar-se que utilitza *Compiler compliance level* 1.8

- A l'Eclipse, instal·lar *e(fx)clipse install*

==== Instal·lació Scene Builder

Oracle només proporciona el codi font de l'Scene Builder. Per evitar
compilar-nos el programa nosaltres mateixos, el podem baixar de
link:http://gluonhq.com/open-source/scene-builder/[Gluon Scene Builder].

A l'Eclipse li hem d'indicar on tenim l'executable de l'Scene Builder. Això es
fa a *Windows->Preferences*, anem a la secció *JavaFX* i a
*SceneBuilder Executable* indiquem la ruta a l'executable de l'Scene Builder.

=== *Hola món* en JavaFX

Per veure les bases de JavaFX podem començar amb analitzar una senzilla
aplicació d'*hola món*. Aquesta aplicació mostra un diàleg amb un botó. Quan
premem aquest botó apareix un diàleg emergent.

El codi és el següent:

link:codi/javafxHelloWorld/src/javafxHelloWorld/HelloWorldFX.java[javafxHelloWorld]

Analitzem per línies:

**Línia 11**: Una aplicació JavaFX sempre deriva de la classe *Application*.

**Línia 13**: l'aplicació gràfica s'arrenca cridant el mètode estàtic
*Application.launch()*.

**Línia 17**: el mètode *launch()* inicialitza el motor gràfic i crida al mètode
*start()* de la nostra *Application*. Noteu que *start()* ja no és estàtic.
Internament, *launch()* ha creat un objecte de la mateixa classe que l'ha
cridat, i després crida *start()* sobre aquest objecte.

El mètode *start()* rep un objecte *Stage*. Aquest *primaryStage* és
l'escenari principal on l'aplicació ha de construir els components gràfics
que necessiti.

**Línies 18 i 19**: creem un botó i li assignem un text.

**Línia 38**: creem un panell on posar els nostres components. El panell
és de tipus *StackPane*, cosa que indica que es posaran els components un a
sobre de l'altre, des del fons cap al davant. En aquest cas ens és útil
perquè l'únic component (el botó) sortirà automàticament centrat.

**Línia 39**: afegim el botó com un dels components de l'*StackPane*.

**Línia 41**: creem l'escena principal. En aquesta aplicació només hi ha una
escena, però en aplicacions més complexes podríem tenir-ne vàries i assignar
a l'escenari quina escena volem mostrar en cada moment. Al constructor
d'*Scene* li passem l'element a mostrar (el panell) i la mida en píxels.

**Línia 43**: posem un títol a la finestra.

**Línia 44**: li diem a l'escenari que mostri l'escena que acabem de crear.

**Línia 45**: fem visible la finestra.

**Línia 21**: assignem l'acció que ha de fer el botó quan es premi.

**Línies 22-27**: creem una finestra emergent amb el títol i text que volem. La
mostrem.

**Línies 29-36**: així s'assigna l'acció al botó si no utilitzem una funció
lambda.

=== Bindings

- link:codi/javafxBindingExample[Exemple de binding]
- link:codi/javafxBindingExample2[Exemple de binding 2]

=== Enllaços externs

- link:http://code.makery.ch/library/javafx-8-tutorial/[Tutorial de JavaFX]
- link:https://docs.oracle.com/javase/8/javafx/api/index.html[Documentació de les API de JavaFX]
- link:http://docs.oracle.com/javase/8/javafx/layout-tutorial/index.html[Tutorial dels layouts disponibles al JavaFX]
